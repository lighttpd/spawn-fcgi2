
Description
-----------

spawn-fcgi prepares the environment for fastcgi applications; it opens a
socket (tcp / unix-domain) on filedescriptor 0, changes uid/gid for process
and unix-domain socket, closes STDIN/STDOUT, ...

Based on spawn-fcgi 1.x from http://redmine.lighttpd.net/projects/spawn-fcgi/wiki,
rewritten, added some extra features:

 * use /etc/security/limits.conf for limits

(The other features were backported to the original spawn-fcgi. Unless you
really need this feature it is recommended to stay with the original,
maintained version.)

Includes modified code from the `Linux-PAM <http://www.kernel.org/pub/linux/libs/pam/>`_
project for the limits.

Usage
-----

Examples for daemontools or runit (with lighttpd on debian):

* for php5::

   #!/bin/sh

   exec 2>&1
   exec /usr/bin/spawn-fcgi -s /var/run/lighttpd/php-webuser.sock -n -u webuser -U www-data -l -- /usr/bin/php5-cgi

* for turbogears::

   #!/bin/sh

   exec 2>&1
   cd /var/www/tgapp
   exec /usr/bin/spawn-fcgi -s /var/run/lighttpd/tgapp.sock -n -u tg-user -U www-data -- /var/www/tg-app/start-tgapp-fcgi.py prod.cfg

 * ``/var/www/tg-app/start-tgapp-fcgi.py``::

    #!/usr/bin/env python
    import pkg_resources
    import cherrypy
    import sys

    cherrypy.lowercase_api = True

    pkg_resources.require("TurboGears")

    from cherrypy._cpwsgi       import wsgiApp
    from flup.server.fcgi       import WSGIServer
    from os.path                import *

    import turbogears

    if len(sys.argv) > 1:
        configfile = sys.argv[1]
    elif exists(join(dirname(__file__), "setup.py")):
        configfile = "dev.cfg"
    else:
        configfile = "prod.cfg"

    turbogears.update_config(configfile=configfile, modulename="tgapp.config")

    from tgapp.controllers import Root

    cherrypy.root = Root()

    cherrypy.server.start(initOnly=True, serverClass=None)

    WSGIServer(application=wsgiApp).run()

License
-------

spawn-fcgi is released under a BSD license, which may be found in the source files:

* `spawn-fcgi.c <http://git.lighttpd.net/spawn-fcgi2.git/tree/spawn-fcgi.c>`_::

   Copyright (c) 2004, Jan Kneschke, incremental
   All rights reserved.

* `pam_limits.c <http://git.lighttpd.net/spawn-fcgi2.git/tree/pam_limits.c>`_::

   Copyright (c) Cristian Gafton, 1996-1997, <gafton@redhat.com>
   All rights reserved.
